import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Apollo, gql } from 'apollo-angular';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CreateBookComponent } from '../create-book/create-book.component';

// import gql from 'graphql-tag';
import { Query } from '../../types';
import { EditBookComponent } from '../edit-book/edit-book.component';
import { DeleteBookComponent } from '../delete-book/delete-book.component';

const GET_BOOKS = gql`
query {
  books {
    id
    title
    author
    price
    stocks
  }
}
`;

export interface Book {
  title: string,
  author: string,
  price: number,
  stocks: number,
}

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})

export class BookComponent implements OnInit {
  public books: Observable<any>;
  public dataSource: any[];
  public loading: Boolean;
  public user: any;
  public displayedData: string[] = [
    'id',
    'title',
    'author',
    'price',
    'stocks'
  ];
  public displayedColumns: string[] = [
    'id',
    'title',
    'author',
    'price',
    'stocks',
    'action'
  ];

  constructor(
    private apollo: Apollo,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log('user', this.user);
    this.books = this.apollo.watchQuery({query: GET_BOOKS})
      .valueChanges.pipe(map((res: any) => {
        this.dataSource = res.data.books;
        this.loading = false;
        return res.data.books;
      }));
  }

  addBook() {
    const config = new MatDialogConfig();
    config.autoFocus = true;
    config.maxWidth = '305px';
    config.maxHeight = '460px';
    config.width = '305px';
    config.height = '460px';
    this.dialog.open(CreateBookComponent, config);
  }

  editBook(book: any) {
    const config = new MatDialogConfig();
    config.autoFocus = true;
    config.maxWidth = '305px';
    config.maxHeight = '460px';
    config.width = '305px';
    config.height = '460px';
    config.data = {
      book
    }
    this.dialog.open(EditBookComponent, config);
  }

  deleteBook(book: any) {
    const config = new MatDialogConfig();
    config.autoFocus = true;
    config.maxWidth = '460px';
    config.maxHeight = '310px';
    config.width = '460px';
    config.height = '310px';
    config.data = {
      book
    }
    this.dialog.open(DeleteBookComponent, config);
  }

  showData() {
    // console.log('ds', this.dataSource);
    console.log('books', this.books);
  }

  logout() {
    console.log('logging out');
    localStorage.clear();
    this.router.navigateByUrl('');
  }
}
