import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Apollo, gql } from 'apollo-angular';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

const LOGIN = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      user{
        id
        firstName
        lastName
        email
      }
      token
    }
  }
`;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading: Boolean = false;
  loginForm: FormGroup;

  constructor(private router: Router, private apollo: Apollo) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup(
      {
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      }
    );
  }

  async onLogin() {
    this.loading = true;
    
    await this.apollo.mutate({
      mutation: LOGIN,
      variables: {
        email: this.loginForm.controls.email.value,
        password: this.loginForm.controls.password.value,
      }
    }).subscribe((res: any) => {
      const user = res.data.login.user;
      const token = res.data.login.token;
      this.loading = false;
      
      localStorage.setItem('user', JSON.stringify(user));
      localStorage.setItem('token', token);
      console.log('login success', token);

      this.router.navigateByUrl('books');
    }, (err) => {
      console.log('login failed', err);
    })
  }

}
