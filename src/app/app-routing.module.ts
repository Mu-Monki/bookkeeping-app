import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { BookComponent } from './components/book/book.component';
import { AppComponent } from './app.component';
import { DataTableComponent } from './data-table/data-table.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { 
    path: '', 
    component: AppComponent,
    children: [
      { path: '', component: LoginComponent },
      { path: 'books', component: BookComponent, canActivate: [AuthGuard] },
      { path: 'table', component: DataTableComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
