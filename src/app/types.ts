export type User = {
    id: string,
    firstName: string,
    lastName: string,
    email: string,
}

export type Book = {
    id: string,
    title: string,
    author: string,
    price: number,
    stocks: number,
}

export type Query = {
    books: Book[];
}