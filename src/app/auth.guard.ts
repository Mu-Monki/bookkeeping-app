import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router
    ) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean {
        
        const token = localStorage.getItem('token');
        const user = localStorage.getItem('user');

        let isAuth = false;

        if(user && token) {
            isAuth = true;
        } else {
            this.router.navigateByUrl('/');
        }

        return isAuth;
    }

    
}