import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Apollo, gql } from 'apollo-angular';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

const ADD_BOOK = gql`
  mutation AddBook($data: BookInput!) {
    addBook(data: $data) {
      id
      title
      author
      price
      stocks
    }
  }
`;

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.scss']
})
export class CreateBookComponent implements OnInit {
  loading: Boolean;
  success: Boolean;
  bookForm: FormGroup;

  constructor(
    private router: Router,
    private apollo: Apollo
  ) { }

  ngOnInit(): void {
    this.bookForm = new FormGroup(
      {
        title: new FormControl('', [Validators.required]),
        author: new FormControl('', [Validators.required]),
        price: new FormControl('', [Validators.required]),
        stocks: new FormControl('', [Validators.required]),
      }
    );
  }

  async addBook() {
    const title = this.bookForm.controls.title.value;
    const author = this.bookForm.controls.author.value;
    const price = this.bookForm.controls.price.value;
    const stocks = this.bookForm.controls.stocks.value;
    const data = {
      title: title,
      author: author,
      price: parseFloat(price),
      stocks: parseInt(stocks)
    };

    this.loading = true;
    console.log('add book', title);

    await this.apollo.mutate({
      mutation: ADD_BOOK,
      variables: {
        data
      }
    }).subscribe((res: any) => {
      this.loading = false;
      this.success = true;
      console.log('book created', res);
    }, (err) => {
      this.loading = false;
      this.success = false;
      console.log('adding failed', err);
    });
  }

  reload() {
    location.reload();
  }
 
}
