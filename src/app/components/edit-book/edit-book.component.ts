import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Apollo, gql } from 'apollo-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';

const UPDATE_BOOK = gql`
  mutation UpdateBook($id: ID!, $data: BookInput!) {
    updateBook(id: $id, data: $data) {
      id
      title
      author
      price
      stocks
    }
  }
`;

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.scss']
})
export class EditBookComponent implements OnInit {

  editForm: FormGroup;
  loading: Boolean = false;
  success: Boolean = false;
  title: string;
  author: string;
  price: Float32Array;
  stocks: Int16Array;


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apollo: Apollo,
    private router: Router
  ) {
    this.loading = true;
    this.title = data.book.title;
    this.author = data.book.author;
    this.price = data.book.price;
    this.stocks = data.book.stocks;

    this.loading = false;
  }

  ngOnInit(): void {
    this.editForm = new FormGroup(
      {
        title: new FormControl('', [Validators.required]),
        author: new FormControl('', [Validators.required]),
        price: new FormControl('', [Validators.required]),
        stocks: new FormControl('', [Validators.required]),
      }
    );
  }

  async updateBook() {
    const id = this.data.book.id;
    const title = this.editForm.controls.title.value;
    const author = this.editForm.controls.author.value;
    const price = parseFloat(this.editForm.controls.price.value);
    const stocks = parseInt(this.editForm.controls.stocks.value);
    const data = {
      title,
      author,
      price,
      stocks
    };

    this.loading = true;
    console.log('update toggled');

    await this.apollo.mutate({
      mutation: UPDATE_BOOK,
      variables: {
        id,
        data
      }
    }).subscribe((res: any) => {
      console.log('book updated', res);
      this.loading = false;
      this.success = true;
    }, (err) => {
      console.log('update failed', err);
      this.loading = false;
    });
  }
}
