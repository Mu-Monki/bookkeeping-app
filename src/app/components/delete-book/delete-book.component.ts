import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Apollo, gql } from 'apollo-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inject } from '@angular/core';

const DELETE_BOOK = gql`
  mutation DeleteBook($id: ID!) {
    deleteBook(id: $id) {
      id
      title
      author
      price
      stocks
    }
  }
`;

@Component({
  selector: 'app-delete-book',
  templateUrl: './delete-book.component.html',
  styleUrls: ['./delete-book.component.scss']
})
export class DeleteBookComponent implements OnInit {
  loading: Boolean;
  success: Boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private apollo: Apollo,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  deleteBook() {
    const id = this.data.book.id;
    this.loading = true;
    console.log('delete called');
    console.log('id', id);

    this.apollo.mutate({
      mutation: DELETE_BOOK,
      variables: {
        id
      }
    }).subscribe((res: any) => {
      console.log('book deleted', res);
      this.loading = false;
      this.success = true;
    }, (err) => {
      console.log('deletion failed', err);
      this.loading = false;
    });
  }

  reload() {
    location.reload(); 
  }

}
